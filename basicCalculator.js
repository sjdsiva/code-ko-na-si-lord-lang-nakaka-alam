import readline from 'readline-sync'

const Array = new Array()

function addition(){
    const a = Array.pop()
    const b = Array.pop()
    const sum = a + b
    console.log(sum)
}

function subtraction(){
    const a = Array.pop()
    const b = Array.pop()
    
    if(a <= b){
        const c = b - a
        console.log(c)
    } else{
        const c = a - b
        console.log(c)
    }
}

function multiplication(){
    const a = Array.pop()
    const b = Array.pop()
    const product =  a * b
    console.log(product)
}

function division(){
    const a = Array.pop()
    const b = Array.pop()
    const quotient = b / a
    console.log(quotient)
}

const input = require(readline)

while(input != '='){
    if(input === '+'){
        addition()
    } else if(input === '-'){
        subtraction()
    } else if(input === '*'){
        multiplication()
    } else if(input === '/'){
        division()
    }
    console.log('=====')
    Array.push(input)
}