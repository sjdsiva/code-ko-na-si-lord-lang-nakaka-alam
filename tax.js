// import readline from "readline-sync";

var income, initialTax, totalTax, total, excemption, dependents, status;

const readline = require("readline-sync");
income = readline.question("Enter Income: ");
dependents = readline.question("Enter Number of Dependents: ");
status = readline.question("Enter Status: ");

if (income < 100001) {
  initialTax = 0.5;
} else if (income >= 100001 && income <= 250000) {
  initialTax = 0.1;
} else if (income >= 250001 && income <= 500000) {
  initialTax = 0.15;
} else {
  initialTax = 0.2;
}
totalTax = income * initialTax;

if (dependents >= 1 && dependents <= 4) {
  dependents = 10000 * dependents;
} else if (dependents == 0) {
  dependents = 10000 * 0;
} else {
  dependents = 10000;
}

excemption = status;

if (status == "S" || status == "s") {
  excemption = 10000;
  status = "Single";
} else if (status == "M" || status == "m") {
  excemption = 20000;
  status = "Married";
} else if (status == "W" || status == "w") {
  excemption = 10000;
  status = "Widowed";
} else {
  status = "idk";
}

total = totalTax - excemption - dependents;
console.log(total);
